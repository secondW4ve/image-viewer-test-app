/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 //Changes bla bla
import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Main from './activities/main';
import ImageView from './activities/imageView';

const Stack = createStackNavigator();

export default class App extends Component {

  render() {

    return (
      <Provider store = {store}>
        <NavigationContainer>
          <Stack.Navigator initialRouteName = "Home" screenOptions={{
              headerShown: false,
            }}
          >
            <Stack.Screen name = "Home" component = { Main }/>
            <Stack.Screen name = "Viewer" component = { ImageView } />
          </Stack.Navigator>
        </NavigationContainer>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  layout: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  box: {
    padding: 25,
    backgroundColor: 'steelblue',
    margin: 5,
  },
});
