import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
    },
    imageOverlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
      },
});

class ProgressiveImage extends React.Component {

    render() {
        const {
            thumbnailSource,
            source,
            style,
            ...props
          } = this.props;
        return (
            <View style = {styles.container}>
                <Image
                    {...props}
                    source={thumbnailSource}
                    style={style}
                />
                <Image
                    {...props}
                    source={source}
                    style={[styles.imageOverlay, style]}
                />
            </View>
        );
    }
}

export default ProgressiveImage;
