import React, { Component } from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';

class ListItem extends Component{


    render(){
        const { item } = this.props;
        let description = item.description === null ? String(item.alt_description) : String(item.description);
        if (description === undefined){
            description = '';
        } else {
            description = description[0].toUpperCase() + description.slice(1);
        }

        return(
            <View style = {styles.container}>
                <TouchableOpacity
                    onPress={this.props.onClickImage}
                >
                    <Image
                        style = {styles.image}
                        source = {{uri: item.urls.small}}
                    />
                </TouchableOpacity>
                <View style = {styles.textContainer}>
                    <Text 
                        style = {styles.username}
                    >
                        {item.user !== null ? item.user.name : "*User was not mentioned*"}
                    </Text>
                    <Text
                        style = {styles.description && ' '}
                    >
                        {description}
                    </Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        borderColor: "#3F5BA0",
        flexDirection: "row",
        borderBottomWidth: 2,
    },
    username: {
        fontSize: 22,
        color: '#d8e0eb',
    },
    description: {
        fontSize: 18,
        padding: 5,
        fontStyle: 'italic',
    },
    image: {
        padding: 10,
        width: 100,
        height: 100,
    },
    textContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        padding: 5,
    }
})

export default ListItem;