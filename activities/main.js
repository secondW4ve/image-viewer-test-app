import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    Text,
    View,
    ActivityIndicator,
    FlatList,
    StyleSheet,
    Button,
    Header,
    } from 'react-native';

import ListItem from '../components/ListItem';
import {getImagesPending, getImages, getImagesError, getPage} from '../redux/reducer';
import fetchImagesAction from '../redux/fetchImages';

class Main extends Component{

    constructor(props) {
        super(props);

        this.shouldComponentRender = this.shouldComponentRender.bind(this);
    }

    componentDidMount () {
        const {fetchImages} = this.props;
        fetchImages();
    }

    shouldComponentRender() {
        const { pending } = this.props;
        if (pending === false) {
            return false;
        }
        return true;
    }

    loadMore = () => {
        const { fetchImages } = this.props;
        fetchImages(this.props.page);
    }

    render(){
        const {images, error, pending} = this.props;
        if (images.length === 0){
            return <ActivityIndicator size="large" color = "#0000ff"/>;
        }
        return (
                <View style = {styles.container}>
                    <View style = {styles.listContainer}>
                        <FlatList
                            data={images}
                            ListFooterComponent = {
                                <Button
                                    title = "Load more"
                                    onPress = {this.loadMore}
                                />
                            }
                            renderItem={({item}) =>
                                <ListItem
                                    item = {item}
                                    onClickImage = {() => this.props.navigation.navigate('Viewer', {selectedImage: item})}
                                />
                            }
                        />
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        backgroundColor: '#2749A0',
    },
    header: {
        padding: 10,
        fontSize: 22,
        textAlign: 'center',
        height: 50,
        fontWeight: 'bold',
        color: '#d8e0eb',
        backgroundColor: '#051F60',
        borderBottomColor: '#3F5BA0',
    },
    listContainer: {
        margin: 15,
    },
});


const mapStateToProps = (state) => ({
    error: getImagesError(state),
    images: getImages(state),
    pending: getImagesPending(state),
    page: getPage(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchImages: fetchImagesAction,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Main);




