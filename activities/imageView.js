import React, { Component } from 'react';
import { View, Image, Text, StyleSheet} from 'react-native';

import ProgressiveImage from '../components/ProgressiveImage';

class ImageView extends Component{


    render(){
        const { selectedImage } = this.props.route.params;
        let thumbnailSource = String(selectedImage.urls.full);
        thumbnailSource += `&w=50&buster=${Math.random()}`;
        return (
            <>
                <ProgressiveImage
                    style = {styles.image}
                    thumbnailSource = {{ uri: thumbnailSource }}
                    source = {{ uri: selectedImage.urls.full }}
                />
                <Text>Some changes</Text>
            </>
        );
    }
}

const styles = StyleSheet.create({
    image: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        resizeMode: 'contain',
    },
});

export default ImageView;
