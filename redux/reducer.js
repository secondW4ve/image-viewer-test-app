import {FETCH_IMAGES_ERROR, FETCH_IMAGES_PENDING, FETCH_IMAGES_SUCCESS} from './actions';

export const initialState = {
    pending: false,
    images: [],
    error: null,
    page: 1,
};

export function rootReducer (state = initialState, action) {
    switch (action.type) {
        case FETCH_IMAGES_PENDING:
            return {
                ...state,
                pending: true,
            };
        case FETCH_IMAGES_SUCCESS:
            const prevImages = state.images;
            const newImages = prevImages.concat(action.payload);
            const nextPage = state.page + 1;
            return {
                ...state,
                pending: false,
                images: newImages,
                page: nextPage,
            };
        case FETCH_IMAGES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
            };
        default:
            return state;
    }
};

export const getImages = state => state.images;
export const getImagesPending = state => state.pending;
export const getImagesError = state => state.error;
export const getPage = state => state.page;
