import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { initialState } from './reducer';
import { rootReducer } from './reducer';


const middleware = [thunk];

export const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));