import {fetchImagesError, fetchImagesPending, fetchImagesSuccess} from './actions';

function fetchImages(page = 1) {
    return dispatch => {
        dispatch(fetchImagesPending());
        fetch(`https://api.unsplash.com/photos/?page=${page}&client_id=N9LxFkq3C7IRAp9UhXdJcLA7KheXuV8RMT_8bvV5WfA`)
        .then(response => response.json())
        .then(response => {
            if (response.errors) {
                throw(response.errors);
            }
            dispatch(fetchImagesSuccess(response));
            return response;
        })
        .catch(error => {
            dispatch(fetchImagesError(error));
        });
    };
}

export default fetchImages;
